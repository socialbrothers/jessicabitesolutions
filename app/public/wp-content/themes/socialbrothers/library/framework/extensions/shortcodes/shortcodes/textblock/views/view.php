<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
    $custom['class'][] = 'textblock';
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
    <?php echo do_shortcode($atts['wysiwyg']); ?>
</div>
