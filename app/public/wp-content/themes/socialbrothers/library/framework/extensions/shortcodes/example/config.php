<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Inhoudselementen', '_SBB'),
		'title'       => __('Voorbeeld', '_SBB'),
		'icon'		  => 'fal fa-heart',
		'popup_size'  => 'medium',
		'disable_correction' => true,
	)
);
