<div class="post">
	<?php if (has_post_thumbnail()) : ?>
	<div class="post__thumbnail">
		<?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full'); ?>
	</div>
	<?php endif; ?>

	<div class="post__content">
		<h6><?php the_title(); ?></h6>
		<p><?php echo excerpt(get_the_content(), 20); ?></p>

		<a class="btn btn--primary" href="<?php echo get_permalink(); ?>">
			<?php echo __('Lees meer', '_SBF'); ?>
		</a>
	</div>
</div>
