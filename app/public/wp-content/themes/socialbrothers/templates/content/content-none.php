<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1><?php echo __('We konden niet vinden waar u naar op zoek was.', '_SBF'); ?></h1>
            <p><?php echo __('Helaas kon de pagina waarnaar u zocht niet worden gevonden. Het kan zijn verplaatst of niet meer bestaan.', '_SBF'); ?></p>
            <p><?php echo __('Controleer de URL die je hebt ingevoerd voor eventuele fouten en probeer het opnieuw. Alternatief, zoek naar wat er ontbreekt of kijk rond op de rest van onze site.', '_SBF'); ?></p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php get_template_part('templates/component/component', 'search'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php menu('primary', 1); ?>
        </div>
    </div>
</div>