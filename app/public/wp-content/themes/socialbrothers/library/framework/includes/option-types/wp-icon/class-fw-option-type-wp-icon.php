<?php if (!defined('FW')) die('Forbidden');

class FW_Option_Type_WPIcon extends FW_Option_Type
{
    private $fields = array(
        'type' => array(
            'type' => 'select',
            'attr' => array(
                'class' => 'wpicon-type',
                'style' => 'margin-bottom:4px;'
            ),
            'choices' => array(
                '' => 'Select',
                'fa-light-300.svg' => 'Light',
                'fa-regular-400.svg' => 'Regular',
                'fa-solid-900.svg' => 'Solid',
                'fa-duotone-900.svg' => 'Duotone',
                'fa-brands-400.svg' => 'Brands',
            )
        ),

        'icons' => array(
            'type' => 'select',
            'attr' => array(
                'disabled' => true,
                'class' => 'wpicon-icons',
                'style' => 'margin-bottom:4px;',
            ),
            'choices' => array()
        ),

        'icon' => array(
            'type' => 'text',
            'attr' => array(
                'class' => 'wpicon-icon',
                'style' => 'display: none;'
            ),
        ),
    );

    public function get_type()
    {
        return 'wp-icon';
    }

    protected function _enqueue_static($id, $option, $data)
    {
    }

    protected function _render($id, $option, $data)
    {
        $html = '<div id="' . $data['id_prefix'] . $id . '" style="display:flex;flex-direction:row;">';

        $html .= '<div class="icon" style="width:64px;height:64px;margin-right:16px;background-color:#f1f1f1;border:1px solid #7e8993;border-radius:3px;display:flex;align-items:center;justify-content:center;font-size:32px;">';
        $html .= '<i class="'. $data['value']['icon'] .'"></i>';
        $html .= '</div>';

        $html .= '<div class="group" style="width: 100%;">';
        foreach ($this->fields as $key => $value) {
            $html .= fw()->backend->option_type($value['type'])->render(
                $key,
                $value,

                array(
                    'value'    => (isset($data['value'][$key])) ? $data['value'][$key] : $option['value'][$key],
                    'id_prefix'   => $data['id_prefix'] . $id . '-',
                    'name_prefix' => $data['name_prefix'] . '[' . $id . ']',
                )
            );
        }
        $html .= '</div>';
        $html .= '</div>';

        // Load the script that checks for changes and loads in the right SVG font.
        $html .= '
		 <script type="text/javascript">
	 		jQuery(document).ready(function() {
                jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons").change(function(e) {
                    e.preventDefault();

                    var type = jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-type option:selected").val();
                    type = type.substr(0, 4).replace("-", "");

                    var icon = jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons option:selected").val();
                    icom = type.replace("-primary", "");

                    jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icon").val(`${type} fa-${icon}`);
                    jQuery("#' . $data['id_prefix'] . $id . ' .icon > i").attr("class", `${type} fa-${icon}`);
                });

                jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-type").change(function() {
                    var type = jQuery(this).val();

                    jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons").prop("disabled", true);
                    jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons option").remove();

                    if (type) {
                        jQuery.ajax({
                            url: "' . get_template_directory_uri() . '/assets/webfonts/" + type,
                            success: function(result) {
                                jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons").prop("disabled", false);

                                var nodes = result.getElementsByTagName("glyph");
                                for (var i = 0; i < nodes.length; i++) {  
                                    var node = nodes[i].getAttribute("glyph-name");
                                    node = node.replace("-primary", "");

                                    jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons").append(`<option value="${node}">${node}</option>`);
                                }

                                jQuery("#' . $data['id_prefix'] . $id . ' .wpicon-icons").trigger("change");
                            }
                        });
                    }
                });
	 		});
         </script>';
         
        return $html;
    }

    protected function _get_value_from_input($option, $input_value)
    {
        if (is_array($input_value)) {
            $value = array();

            $value['icon'] = '';

            if (isset($input_value['icon'])) {
                $value['icon'] = $input_value['icon'];
            }

            return $value;
        }

        return $option['value'];
    }

    protected function _get_defaults()
    {
        return array(
            'value' => array(
                'icon' => 'fab fa-wordpress'
            )
        );
    }
}

FW_Option_Type::register('FW_Option_Type_WPIcon');
