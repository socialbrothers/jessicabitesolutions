<?php if (!defined('FW')) die('Forbidden');

$posts = get_post_types(array(
	'public' => true,
	'publicly_queryable' => true
), 'objects');


if ($posts) {
	$select = array();

	foreach ($posts as $key => $post) {
		$select[$key] = $post->labels->name;
	}
}


$args = array(
    'post_type' => array(
        'label' => __('Selecteer een berichttype', '_SBB'),
        'type' => 'select',
        'choices' => $select,
    ),

    'posts_per_page' => array(
        'label' => __('Aantal berichten?', '_SBB'),
        'type'  => 'slider',
        'value' => 6,
        'properties' => array(
            'min' => 1,
            'max' => 12,
        ),
    ),

    'posts_per_row' => array(
        'label' => __('Aantal berichten per rij?', '_SBB'),
        'type'  => 'slider',
        'value' => 4,
        'properties' => array(
            'min' => 1,
            'max' => 4,
        ),
    ),

    'slider' => array(
        'label' => __('Weergeven in een carrousel?', '_SBB'),
        'type' => 'switch',
    ),

    'navigation' => array(
        'label' => __('Pijltjes tonen?', '_SBB'),
        'type' => 'switch'
    ),

    'bullets' => array(
        'label' => __('Bolletjes tonen?', '_SBB'),
        'type' => 'switch'
    ),

    'autoplay' => array(
        'label' => __('Automatisch afspelen?', '_SBB'),
        'value' => 2500,
        'desc' => __('Geef hier aan hoeveel milliseconden een afbeelding wordt weergegeven', '_SBB'),
        'type' => 'text'
    )
);

$options = fw_get_defaults($args);