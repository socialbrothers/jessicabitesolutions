<?php if (!defined('FW')) die('Forbidden');

$sizes['xs'] = array('title' => __('Extra klein', '_SBB'), 'desc' => __('Extra kleine toestellen (portrait phones)', '_SBB'));
$sizes['sm'] = array('title' => __('Klein', '_SBB'), 'desc' => __('Kleine toestellen  (landscape phones)', '_SBB'));
$sizes['md'] = array('title' => __('Gemiddeld', '_SBB'), 'desc' => __('Gemiddelde toestellen (desktops)', '_SBB'));
$sizes['lg'] = array('title' => __('Groot', '_SBB'), 'desc' => __('Grote toestellen (desktops)', '_SBB'));
$sizes['xl'] = array('title' => __('Extra groot', '_SBB'), 'desc' => __('Extra grote toestellen (large desktops)', '_SBB'));

$opts = array();

foreach($sizes as $key => $value) {
    $opts[$key] = array(
        'label'   => $value['title'],
        'desc'    => $value['desc'],
        'type'    => 'select',
        'choices' => array()
	);

	if ($key == 'lg') {
		$opts[$key]['choices'][''] = __('Standaard kolom grootte', '_SBB');
	} else if ($key == 'xl') {
		$opts[$key]['choices'][''] = __('Neem over van onderstaande', '_SBB');
	} else {
	    $opts[$key]['choices'][''] = __('Neem over van bovenstaande', '_SBB');
	}

	if ($key != 'lg') {
		for($i = 1; $i <= 12; $i++) {
	        $opts[$key]['choices']['col-' . $key . '-' . $i] = 'Kolom ' . $i . '/12';
	    }
	}
}

$args = array(
    $opts
);

$options = fw_get_defaults($args);