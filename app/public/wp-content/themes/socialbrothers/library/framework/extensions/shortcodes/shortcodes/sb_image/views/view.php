<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
	$custom['class'][] = 'figure';
?>

<?php if ($atts['image']) : ?>
    <figure <?php echo fw_get_defaults_settings($atts, $custom); ?>>
        <?php echo wp_get_attachment_image($atts['image']['attachment_id'], 'full'); ?>
    </figure>
<?php endif; ?>
