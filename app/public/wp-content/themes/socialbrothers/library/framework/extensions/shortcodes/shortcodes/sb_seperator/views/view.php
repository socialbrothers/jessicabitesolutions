<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
    $custom['class'][] = 'seperator';
    $custom['class'][] = (($atts['line'] == true) ? 'seperator--line' : '');
    $custom['class'][] = (($atts['line'] == true) ? 'mtb-' : 'mt-') . esc_attr($atts['size']);
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>></div>
