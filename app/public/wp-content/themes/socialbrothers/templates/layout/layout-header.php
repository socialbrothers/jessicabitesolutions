<header class="header">
	<div class="header__wrapper">
		<a class="header__brand" href="<?php echo esc_url(home_url('/')); ?>">
			<img src="<?php echo assets('images/logo.svg'); ?>" alt="<?php bloginfo('name'); ?>">
		</a>

		<div class="header__navigation">
			<div class="header__menu">
				<?php menu('primary', 2, true); ?>
			</div>
		</div>

		<div class="header__toggler toggler">
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div>
</header>

<main class="body">