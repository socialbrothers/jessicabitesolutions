<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
	$custom['class'][] = 'form';
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
	<?php echo do_shortcode('[gravityform id='.$atts['form'].' title='.$atts['title'].' description='.$atts['description'].' ajax='.$atts['ajax'].']'); ?>
</div>