<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Inhoudselementen', '_SBB'),
		'title'       => __('Tekstblok', '_SBB'),
		'title_template' => '{{- o.wysiwyg.length > 0 ? o.wysiwyg.replace(/<(?:.|\n)*?>/gm, "").substr(0, 50) + "..." : ' . __('Tekstblok', '_SBB') . ' }}',
		'icon'		  => 'fal fa-file-alt',
        'popup_size'  => 'medium',
        'disable_correction' => true,
	)
);
