<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'fluid' => array(
        'label'		=> __('Volledige breedte', '_SBB'),
        'value'		=> false,
        'desc'		=> __('Wilt u dat de sectie de volledige breedte van de website inneemt?', '_SBB'),
        'type'      => 'switch',
    ),

    'rows' => array(
        'label'		=> __('Rijen toestaan', '_SBB'),
        'value'		=> true,
        'desc'		=> __('Wilt u dat de sectie rijen heeft (soms niet vanwege responsiveness)?', '_SBB'),
        'type'      => 'switch',
    ),

    'center_horizontal' => array(
        'label'		=> __('Midden uitlijnen (horitonzaal)', '_SBB'),
        'value'		=> false,
        'desc'		=> __('Wilt u dat de inhoud van de sectie in het midden wordt uitgelijnd (horitonzaal)?', '_SBB'),
        'type'      => 'switch',
    ),

    'center_vertical' => array(
        'label'		=> __('Midden uitlijnen (verticaal)', '_SBB'),
        'value'		=> false,
        'desc'		=> __('Wilt u dat de inhoud van de sectie in het midden wordt uitgelijnd (verticaal)?', '_SBB'),
        'type'      => 'switch',
    ),

    'alt' => array(
        'label'		=> __('Alternatieve kleur', '_SBB'),
        'value'		=> false,
        'desc'		=> __('Wilt u dat de sectie een alternatieve kleur heeft?', '_SBB'),
        'type'      => 'switch',
    )
);

$options = fw_get_defaults($args);