</main>

<footer class="footer" role="contentinfo">
	<div class="footer__bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
                    <?php printf(esc_html__('&copy; %s %s. All Rights Reserved.', '_SBF'), get_bloginfo('name'), date('Y'));  ?>
				</div>
			</div>
		</div>
	</div>
</footer>