<?php if(function_exists('fw_ext_page_builder_is_builder_post') && fw_ext_page_builder_is_builder_post(get_the_ID())): ?>
    <?php the_content(); ?>
<?php else: ?>
    <section class="section section--first">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>