<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'categorys' => array(
        'type'          => 'addable-popup',
        'label'         => __('Categorieën', '_SBB'),
        'popup-title'   => __('Categorieën', '_SBB'),
        'template'      => '{{=category_title}}',
        'popup-options' => array(
            'category_title'   => array(
                'type'  => 'text',
                'label' => __('Titel', '_SBB')
            ),

            'category_items' => array(
                'type'          => 'addable-popup',
                'label'         => __('Blokken', '_SBB'),
                'popup-title'   => __('Voeg een blok toe', '_SBB'),
                'template'      => '{{=item_title}}',
                'popup-options' => array(
                    'item_title'   => array(
                        'type'  => 'text',
                        'label' => __('Titel', '_SBB')
                    ),
                    'item_content' => array(
                        'type'  => 'wp-editor',
                        'label' => __('Inhoud', '_SBB')
                    )
                )
            )
        )
    )
);

$options = fw_get_defaults($args);