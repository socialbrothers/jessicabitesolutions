<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'tab'         => __('Inhoudselementen', '_SBB'),
        'title'       => __('Carrousel', '_SBB'),
        'icon'		  => 'fal fa-images',
        'popup_size'  => 'medium',
        'disable_correction' => true,
    )
);
