<?php if (!defined('ABSPATH')) die('Forbidden');

/**
 * This function will return the URI of the needed file passed through as
 * parameter. It will look in the assets folder so this function is nice to use
 * if you need to load a asset.
 *
 * @param string $file
 * @return null
 */
function assets($file)
{
    return get_template_directory_uri() . '/assets/' . $file;
}

/**
 * This function will return a correct anchor link with the input
 * you put in.
 *
 * @param array $args
 * @param array $extra
 *
 * @return string
 */
function anchor($args, $extra = array())
{
    $attr['href'] = $args['href'];

    if ($args['target']) {
        $attr['target'] = '_blank';
        $attr['rel'] = 'noopener noreferrer';
    }

    return '<a ' . attributes(array_merge($attr, $extra)) . '>' . do_shortcode($args['text']) . '</a>';
}

/**
 * This function will convert an array to correct HTML attributes
 * which you can put on an element.
 *
 * @param array $attr
 *
 * @return array
 */
function attributes($attr)
{
    if (empty($attr) || !is_array($attr)) {
        return '';
    }

    $pairs = array();

    foreach ($attr as $key => $val) {
        if (is_int($key))
            $pairs[] = $val;
        else {
            $val = htmlspecialchars($val, ENT_QUOTES);
            $pairs[] = "{$key}=\"{$val}\"";
        }
    }

    return join(' ', $pairs);
}

/**
 * This function will return the a custom excerpt. You can set a custom word limit and
 * a text that will be placed after the text needs to be trimmed.
 *
 * @param string $content
 * @param int $limit
 * @param string $more
 * @return string
 */
function excerpt($content, $limit = 55, $more = '...')
{
    return wp_trim_words(strip_tags($content), $limit, $more);
}

/**
 * This function will nicely output a result where you can easily see what
 * happends without messing with your layout.
 *
 * @param string $content
 * @return null
 */
function dump($content)
{
    echo '<pre><code>';
    print_r($content);
    echo '</code></pre>';
}

/**
 * This function will return our custom BEM WordPress menu instead of the standard
 * and default WordPress menu.
 *
 * @param string $location
 * @param int $depth
 * @param string $classes
 * @return object
 */
function menu($location = 'primary', $depth = 2, $classes = false)
{
    $args = array(
        'theme_location' => $location,
        'depth' => $depth,
        'walker' => new Walker_Bem($classes ? 'menu--horizontal' : 'menu--vertical'),
    );

    if (has_nav_menu($location)) {
        return wp_nav_menu($args);
    } else {
        echo sprintf('You need to first define a menu in %s.', $location);
    }
}

/**
 * This function will return the modifed time of the file. If there is none it will return
 * the current time.
 *
 * @param int
 */
function filetime($path)
{
    $time = filemtime($path);
    if ($time)
        return $time;

    return time();
}

