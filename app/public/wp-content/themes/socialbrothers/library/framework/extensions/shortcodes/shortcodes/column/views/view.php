<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
	$custom['class'][] = fw_ext_builder_get_item_width('page-builder', $atts['width'] . '/frontend_class');
	$custom['class'][] = ($atts['xs'] != '' ? ' ' . str_replace('-xs', '', $atts['xs']) : '');
	$custom['class'][] = ($atts['sm'] != '' ? ' ' . $atts['sm'] : '');
	$custom['class'][] = ($atts['md'] != '' ? ' ' . $atts['md'] : '');
	$custom['class'][] = ($atts['xl'] != '' ? ' ' . $atts['xl'] : '');
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
	<div class="inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>