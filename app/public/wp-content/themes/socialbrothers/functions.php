<?php if (!defined('ABSPATH')) die('Forbidden');

// First we will load our vendor items, these will be used throughout the WordPress theme.
require get_template_directory() . '/library/vendor/class-walker-nav-bem.php';

// Here we will clean up all the ugly stuff that WordPress has created for us.
require get_template_directory() . '/library/theme-init.php';

// Here we will register some functions that are needed to succesfully initialize WordPress theme.
require get_template_directory() . '/library/theme-functions.php';

// Here we will register helper functions that can be used throughout the WordPress website.
require get_template_directory() . '/library/theme-helpers.php';

// Here we will register our custom AJAX functions.
require get_template_directory() . '/library/theme-ajax.php';

// Here we will register widget areas, navigation menus and post types.
require get_template_directory() . '/library/theme-register.php';

// Here we will register widget areas, navigation menus and post types.
require get_template_directory() . '/library/theme-plugins.php';

// Here we will register custom functions for the TinyMCE-editor.
require get_template_directory() . '/library/theme-editor.php';
