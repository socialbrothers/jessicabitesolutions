<?php

if (!defined('FW')) die('Forbidden'); ?>

<?php
$query = new WP_Query(array(
    'post_type' => $atts['post_type'],
    'posts_per_page' => $atts['posts_per_page']
));

$custom['class'][] = 'posts';
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
    <?php if ($query->have_posts()) : ?>
        <?php if ($atts['slider']) : ?>
            <?php
                    $swiper = array(
                        'spaceBetween' => 30,
                        'slidesPerView' => (int) $atts['posts_per_row'],
                        'autoplay' => array(
                            'delay' => (int) $atts['autoplay']
                        ),
                        'breakpoints' => array(
                            '1140' => array(
                                'slidesPerView' => (int) $atts['posts_per_row']
                            ),

                            '991' => array(
                                'slidesPerView' => (int) $atts['posts_per_row'] >= 2 ? 2 : 1
                            ),

                            '720' => array(
                                'slidesPerView' => 1
                            ),
                        )
                    );
                    ?>

            <div class="slider <?php echo ($atts['posts_per_row'] > 1 && $atts['navigation'] ? 'slider--buttons-outside' : ''); ?>">
                <div class="slider__swiper swiper-container" data-swiper='<?php echo json_encode($swiper, JSON_FORCE_OBJECT); ?>'>
                    <div class="slider__wrapper swiper-wrapper">
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <div class="slider__slide swiper-slide">
                                <?php get_template_part('templates/component/component', 'post'); ?>
                            </div>
                        <?php endwhile; ?>
                    </div>

                    <?php if ($atts['navigation']) : ?>
                        <div class="slider__button slider__button--prev">
                            <i class="fas fa-chevron-left"></i>
                        </div>

                        <div class="slider__button slider__button--next">
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    <?php endif; ?>

                    <?php if ($atts['bullets']) : ?>
                        <div class="slider__pagination"></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php else : ?>
            <div class="posts__grid">
                <div class="row">
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="col-md-<?php echo 12 / $atts['posts_per_row']; ?>">
                            <?php get_template_part('templates/component/component', 'post'); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>