<?php if (!defined('FW')) die('Forbidden');

if (class_exists('GFCommon')) {
	$cfg = array(
		'page_builder' => array(
			'tab'         => __('Inhoudselementen', '_SBB'),
			'title'       => __('Formulier', '_SBB'),
			'icon'		  => 'fal fa-keyboard',
            'popup_size'  => 'medium',
            'disable_correction' => true,
		)
	);
}
