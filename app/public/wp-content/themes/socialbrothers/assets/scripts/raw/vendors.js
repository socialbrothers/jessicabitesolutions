/**
 * Toggle's the submenu when the user clicks on a parent menu link item,
 * when clicking somewhere else it hides the submenu again.
 */
function menu() {
	const dropdown = $(".menu .menu__item--has-children");
	const active = "menu__item--active";

	dropdown.children(".menu__link").on("click", function (e) {
		e.preventDefault();
		e.stopPropagation();

		// Remove active classes from all item's
		dropdown.not($(this).parent()).removeClass(active);

		// Add active class to current click
		$(this).parent().toggleClass(active);
	});

	// Remove active class when clicked somewhere else
	$(document).click(() => {
		$(".menu .menu__item").removeClass(active);
	});
}

menu();

/**
 * If mobile it opens the navigation as sidebar, when clicking somewhere else
 * when the sidebar is open it hides automatically.
 */
function navigation() {
	const active = "header--active";
	const toggler = "toggler--active";

	// Add active class to current click
	$(document).on("click", ".header .header__toggler", (e) => {
		$(".header").toggleClass(active);
		$(".header__toggler").toggleClass(toggler);
	});

	$(document).on(
		"click",
		".header .header__navigation .header__menu, .header .header__toggler",
		(e) => {
			e.stopPropagation();
		}
	);

	// Remove active class when clicked somewhere else
	$(document).click(() => {
		$(".header").removeClass(active);
		$(".header__toggler").removeClass(toggler);
	});
}

navigation();

/**
 * Slider
 */
function slider() {
	const sliders = document.querySelectorAll(".slider .slider__swiper");

	Array.prototype.slice.call(sliders).forEach((slide) => {
		const args = {
			loop: true,
			speed: 500,
			autoHeight: true,

			pagination: {
				el: slide.querySelector(".slider__pagination"),
				clickable: true,
			},

			navigation: {
				nextEl: slide.querySelector(".slider__button--next"),
				prevEl: slide.querySelector(".slider__button--prev"),
			},
		};

		if (slide.hasAttribute("data-swiper")) {
			const attr = JSON.parse(slide.getAttribute("data-swiper"));
			new Swiper(slide, { ...attr, ...args });
		} else {
			new Swiper(slide, args);
		}
	});
}

slider();

/**
 * File Upload
 */
function fileupload(elem) {
	const { name } = elem.files[0];
	elem.parentElement.querySelector(".upload__result").innerHTML = name;
}

/**
 * Object-fit Polyfill for images
 */
$(function () {
	objectFitImages();
});
