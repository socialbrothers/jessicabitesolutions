<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
	$container = '';
	$container .= ($atts['fluid'] == true ? 'container-fluid' : 'container');

    $custom['class'][] = 'section';
    $custom['class'][] = (isset($atts['first_in_builder']) ? 'section--first' : '');
	$custom['class'][] = ($atts['alt'] != '' ? 'section--alt' : '');
	$custom['class'][] = ($atts['center_horizontal'] != '' ? 'section--center-horizontal' : '');
    $custom['class'][] = ($atts['center_vertical'] != '' ? 'section--center-vertical' : '');
?>

<section <?php echo fw_get_defaults_settings($atts, $custom); ?>>
	<div class="<?php echo esc_attr($container); ?>">
		<?php
		if ($atts['rows'] == true) {
			echo do_shortcode($content);
		} else {
			$content = str_replace('[row]', '', $content);
			$content = str_replace('[/row]', '', $content);

			echo  '<div class="row">' . do_shortcode($content) . '</div>';
		}
		?>
	</div>
</section>