<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'wysiwyg'  => array(
        'label' => __('Inhoud', '_SBB'),
        'type' => 'wp-editor',
        'editor_height' => 250,
    )
);

$options = fw_get_defaults($args);