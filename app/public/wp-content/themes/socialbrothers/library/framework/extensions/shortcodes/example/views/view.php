<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
    dump($atts);
    $custom['class'][] = 'example';
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
    <div class="example__title">
        <?php echo 'Hello, I am the "' . $custom['class'][0] . '" shortcode!' ?>
    </div>
</div>
