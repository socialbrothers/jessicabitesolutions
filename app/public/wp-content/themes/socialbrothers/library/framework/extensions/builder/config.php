<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

/**
 * Default item widths for all builders
 *
 * It is better to use fw_ext_builder_get_item_width() function to retrieve the item widths
 * because it has a filter and users will be able to customize the widths for a specific builder
 *
 * @see fw_ext_builder_get_item_width()
 * @since 1.2.0
 *
 * old $cfg['default_item_widths'] https://github.com/ThemeFuse/Unyson-Builder-Extension/issues/8
 * https://github.com/ThemeFuse/Unyson-Builder-Extension/blob/v1.1.17/config.php#L13
 */

$cfg['grid.columns'] = array(
	'1_12' => array(
		'title'          => '1',
		'backend_class'  => 'fw-col-sm-1',
		'frontend_class' => 'col-lg-1',
	),
	'2_12' => array(
		'title'          => '2',
		'backend_class'  => 'fw-col-sm-2',
		'frontend_class' => 'col-lg-2',
	),
	'3_12' => array(
		'title'          => '3',
		'backend_class'  => 'fw-col-sm-3',
		'frontend_class' => 'col-lg-3',
	),
	'4_12' => array(
		'title'          => '4',
		'backend_class'  => 'fw-col-sm-4',
		'frontend_class' => 'col-lg-4',
	),
	'5_12' => array(
		'title'          => '5',
		'backend_class'  => 'fw-col-sm-5',
		'frontend_class' => 'col-lg-5',
	),
	'6_12' => array(
		'title'          => '6',
		'backend_class'  => 'fw-col-sm-6',
		'frontend_class' => 'col-lg-6',
	),
	'7_12' => array(
		'title'          => '7',
		'backend_class'  => 'fw-col-sm-7',
		'frontend_class' => 'col-lg-7',
	),
    '8_12' => array(
        'title'          => '8',
        'backend_class'  => 'fw-col-sm-8',
        'frontend_class' => 'col-lg-8',
    ),
    '9_12' => array(
        'title'          => '9',
        'backend_class'  => 'fw-col-sm-9',
        'frontend_class' => 'col-lg-9',
    ),
    '10_12' => array(
        'title'          => '10',
        'backend_class'  => 'fw-col-sm-10',
        'frontend_class' => 'col-lg-10',
    ),
    '11_12' => array(
        'title'          => '11',
        'backend_class'  => 'fw-col-sm-11',
        'frontend_class' => 'col-lg-11',
    ),
    '12_12' => array(
        'title'          => '12',
        'backend_class'  => 'fw-col-sm-12',
        'frontend_class' => 'col-lg-12',
    ),
);

/**
 * @since 1.2.0
 */
$cfg['grid.row.class'] = 'row';

/**
 * @deprecated since 1.2.0
 * if this is empty fw_ext_builder_get_item_width() will use $cfg['grid.columns']
 */
$cfg['default_item_widths'] = false;
