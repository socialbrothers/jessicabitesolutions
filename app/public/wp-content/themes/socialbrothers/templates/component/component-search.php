<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
	<div class="input-group">
		<input type="search" class="form-control search-field" placeholder="<?php echo __('Waar bent u naar opzoek?', '_SBF') ?>" value="<?php echo get_search_query() ?>" name="s" />

		<span class="input-group-btn">
			<button type="submit" class="btn btn-secondary search-submit"><?php echo __('Zoeken', '_SBF'); ?></button>
		</span>
	</div>
</form>
