<?php if (!defined('FW')) die('Forbidden'); ?>

<?php

$swiper = array();

if ($atts['posts_per_row'] > 1) {
	$swiper = array(
		'spaceBetween' => 30,
		'slidesPerView' => (int) $atts['posts_per_row'],
		'autoplay' => array(
			'delay' => (int) $atts['autoplay']
		),
		'breakpoints' => array(
			'1140' => array(
				'slidesPerView' => (int) $atts['posts_per_row']
			),

			'991' => array(
				'slidesPerView' => (int) $atts['posts_per_row'] >= 2 ? 2 : 1
			),

			'720' => array(
				'slidesPerView' => 1
			),
		)
	);
}

$custom['class'][] = 'slider';

if ( $atts['posts_per_row'] > 1 && $atts['navigation'] ) {
    $custom['class'][] = 'slider--buttons-outside';
}
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
	<div class="slider__swiper swiper-container" data-swiper='<?php echo json_encode($swiper, JSON_FORCE_OBJECT); ?>'>
		<div class="swiper-wrapper">
			<?php foreach ($atts['slides'] as $slide) : ?>
			<div class="slider__slide swiper-slide">
				<?php echo wp_get_attachment_image($slide['attachment_id'], 'full'); ?>
			</div>
			<?php endforeach; ?>
		</div>

		<?php if ($atts['navigation']) : ?>
		<div class="slider__button slider__button--prev">
			<i class="fas fa-chevron-left"></i>
		</div>

		<div class="slider__button slider__button--next">
			<i class="fas fa-chevron-right"></i>
		</div>
		<?php endif; ?>

		<?php if ($atts['bullets']) : ?>
		<div class="slider__pagination"></div>
		<?php endif; ?>
	</div>
</div>
