<?php if (!defined('ABSPATH')) die('Forbidden');

function example()
{
    $query = new WP_Query(array(
        'post_type' => $_POST['post_type'],
        'posts_per_page' => $_POST['posts_per_page'],
        'offset' => $_POST['post_offset']
    ));

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();

            get_template_part('templates/component/case', 'large');
        }
    }

    wp_reset_query();
    die();
}

// add_action('wp_ajax_example', 'example');
// add_action('wp_ajax_nopriv_example', 'example');