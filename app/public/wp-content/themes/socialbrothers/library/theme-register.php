<?php if (!defined('ABSPATH')) die('Forbidden');

/**
 * Here we will register our widget areas that will be used throughout
 * the entire WordPress website.
 *
 * @return null
 */
function widgets_init()
{
    $args = array(
        'name'          => __('Widget 1', '_SBB'),
        'id'            => 'widget-1',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    );

    register_sidebar($args);
}

// add_action( 'widgets_init', 'widgets_init' );

/**
 * Here we will register our navigation menus that will be used throughout
 * the entire WordPress website.
 *
 * @return null
 */
function menus_init()
{
    register_nav_menus(array(
        'primary' => __('Hoofdmenu', '_SBB'),
    ));
}

add_action('after_setup_theme', 'menus_init');

/**
 * Here we will register our custom post types that will be used throughout
 * the entire WordPress website.
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type
 *
 * @return null
 */
function posts_init()
{
    $books = array(
        'labels' => array(
            'name' => __('Boeken', '_SBB'),
            'singular_name' => __('Boek', '_SBB'),
        ),

        'menu_icon' => 'dashicons-welcome-learn-more',
        'menu_position' => 22,

        'public' => true,
        'has_archive' => false,
        'supports' => array('title', 'editor', 'thumbnail'),
    );

    register_post_type('boeken', $books);
}

// add_action('init', 'posts_init');

/**
 * Here we will register our custom taxonomies that will be used throughout
 * the entire WordPress website.
 *
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
 *
 * @return null
 */
function taxonomies_init()
{
    $categories = array(
        'labels' => array(
            'name' => __('Categorieën', '_SBB'),
            'singular_name' => __('Categorie', '_SBB'),
        ),

        'public' => false,
        'show_ui' => true,
        'rewrite' => false,
        'hierarchical' => true,
    );

    register_taxonomy('category', array('boeken'), $categories);
}

// add_action('init', 'taxonomies_init');

/**
 * Here we will register custom image sizes, such as thumbnails and images for custom
 * elements. This will speed up the load time of the website.
 *
 * @link https://developer.wordpress.org/reference/functions/add_image_size/
 *
 * @return null
 */
function images_init()
{
    $container = 1170;

    add_image_size('xs', ceil($container / 5 * 1), ceil($container / 5 * 1) * 10, false);
    add_image_size('sm', ceil($container / 5 * 2), ceil($container / 5 * 2) * 10, false);
    add_image_size('md', ceil($container / 5 * 3), ceil($container / 5 * 3) * 10, false);
    add_image_size('lg', ceil($container / 5 * 4), ceil($container / 5 * 4) * 10, false);
    add_image_size('xl', ceil($container / 5 * 5), ceil($container / 5 * 5) * 10, false);
}

// add_action('after_setup_theme', 'images_init');