<?php if (!defined('ABSPATH')) die('Forbidden');

/**
 * With this function we will clean up all the ugly stuff that WordPress has
 * created for us, it calls functions that are located in our theme-functions file.
 *
 * @return null
 */
function init()
{
	// Cleanup and adjust the WordPress Header
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'rel_canonical', 10, 0);
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');

	// Add our unique scripts to the WordPress Header
	add_action('wp_enqueue_scripts', 'scripts');

	// Add custom CSS to the WordPress Backend
	add_action('admin_enqueue_scripts', 'admin_scripts');

	// Lets register some theme specific elements for WordPress
	add_theme_support('menus');
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('automatic-feed-links');
	add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));

	// Change our WordPress Backend Login with a custom logo and URL
	add_filter('login_headerurl', 'login_url');

	// Disable WordPress REST API
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

	add_filter('json_enabled', '__return_false');
	add_filter('json_jsonp_enabled', '__return_false');

	// Disable WordPress oEmbed
	add_filter('embed_oembed_discover', '__return_false');
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links');
	remove_action('wp_head', 'wp_oembed_add_host_js');

	// Disable WordPress Theme/Plugin Editor
    define('DISALLOW_FILE_EDIT', true);
}

add_action('after_setup_theme', 'init');
