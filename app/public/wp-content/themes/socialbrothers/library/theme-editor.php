<?php if (!defined('ABSPATH')) die('Forbidden');

/**
 * Register shortcodes to the style selector within the Tiny-MCE editor.
 *
 * @param $settings
 */
function tiny_mce_shortcodes($settings)
{
	$style_formats = array(
        array(
			'title'   => __('Titels', '_SBB'),
			'items' => array(
				array(
					'title'    => __('Heading 1', '_SBB'),
					'selector' => 'h1,h2,h3,h4,h5,h6',
					'classes'  => 'h1',
				),
				array(
					'title'    => __('Heading 2', '_SBB'),
					'selector' => 'h1,h2,h3,h4,h5,h6',
					'classes'  => 'h2',
				),
				array(
					'title'    => __('Heading 3', '_SBB'),
					'selector' => 'h1,h2,h3,h4,h5,h6',
					'classes'  => 'h3',
				),
				array(
					'title'    => __('Heading 4', '_SBB'),
					'selector' => 'h1,h2,h3,h4,h5,h6',
					'classes'  => 'h4',
				),
				array(
					'title'    => __('Heading 5', '_SBB'),
					'selector' => 'h1,h2,h3,h4,h5,h6',
					'classes'  => 'h5',
				),
				array(
					'title'    => __('Heading 6', '_SBB'),
					'selector' => 'h1,h2,h3,h4,h5,h6',
					'classes'  => 'h6',
				),
			)
        ),
        
		array(
			'title'   => __('Tekst', '_SBB'),
			'items' => array(
				array(
					'title'    => __('Intro', '_SBB'),
					'selector' => 'p',
					'classes'  => 'lead',
                ),

                array(
					'title'    => __('Klein', '_SBB'),
					'selector' => 'p',
					'classes'  => 'small',
				)
			)
		),

		array(
			'title'   => __('Knoppen', '_SBB'),
			'items' => array(
				array(
					'title'    => __('Primair', '_SBB'),
					'selector' => 'a',
					'classes'  => 'btn btn--primary',
				),

				array(
					'title'    => __('Primair omlijning', '_SBB'),
					'selector' => 'a',
					'classes'  => 'btn btn--primary-outline',
				),

				array(
					'title'    => __('Primair tekst', '_SBB'),
					'selector' => 'a',
					'classes'  => 'btn btn--primary-text',
				),

				array(
					'title'    => __('Secundair', '_SBB'),
					'selector' => 'a',
					'classes'  => 'btn btn--secondary',
				),

				array(
					'title'    => __('Secundair omlijning', '_SBB'),
					'selector' => 'a',
					'classes'  => 'btn btn--secondary-outline',
				),

				array(
					'title'    => __('Secundair tekst', '_SBB'),
					'selector' => 'a',
					'classes'  => 'btn btn--secondary-text',
				)
			)
		)
    );
    
    $settings['style_formats_merge'] = false;
    $settings['style_formats'] = json_encode($style_formats);

    return $settings; 
}

add_filter('tiny_mce_before_init', 'tiny_mce_shortcodes');

/**
 * Register colors to the color picker within the Tiny-MCE editor.
 *
 * @param $init
 */
function tiny_mce_colors($init)
{
	$colors = '
		"267293", "Primary",
		"6ba3bb", "Secondary",
		"6bb39d", "Alternative"
	';

	$init['textcolor_map'] = '[' . $colors . ']';
	$init['textcolor_rows'] = 6;

	return $init;
}
add_filter('tiny_mce_before_init', 'tiny_mce_colors');

/**
 * Register button to select a style within the TinyMCE-editor.
 *
 * @param $buttons
 */
function tiny_mce_buttons($buttons)
{
	array_unshift($buttons, 'styleselect');
	return $buttons;
}

add_filter('mce_buttons_2', 'tiny_mce_buttons');

/**
 * Custom styling for the TinyMCE-editor, so we can make like the lead
 * paragraph a little bit larger.
 */
function tiny_mce_styling()
{
	add_editor_style('library/framework/editor.css');
}

add_action('admin_init', 'tiny_mce_styling');

/**
 * Shortcodes to use within your theme. You have to wrap the shortcode within
 * do_shortcode() functtion
 */
function icon__shortocode($atts) {
    if (isset($atts) && isset($atts['icon']))
        return '<i class="icon '.$atts['icon'].'"></i>';
}

add_shortcode('icon', 'icon__shortocode');


/**
 * Disable gutenburg
 * 
 */
add_filter( 'use_block_editor_for_post', '__return_false' );