<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => __('Sectie', '_SBB'),
		'description' => __('Voeg een Sectie toe', '_SBB'),
		'type'        => 'section',
		'icon'		  => 'fal fa-cube'
	)
);
