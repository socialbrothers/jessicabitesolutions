<?php if (!defined('FW')) die('Forbidden'); ?>

<?php
	$custom['class'][] = 'accordion';
?>

<div <?php echo fw_get_defaults_settings($atts, $custom); ?>>
	<?php foreach ($atts['categorys'] as $category) : ?>
	<div class="accordion__group">
		<h4 class="accordion__category"><?php echo $category['category_title']; ?></h4>

		<div class="accordion__items">
			<?php foreach ($category['category_items'] as $item) : ?>
			<div class="accordion__item">
				<div class="accordion__header">
					<h5 class="accordion__title"><?php echo $item['item_title']; ?></h5>
				</div>

				<div class="accordion__content">
					<?php echo $item['item_content']; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endforeach; ?>
</div>

