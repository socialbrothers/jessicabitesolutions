<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'navigation' => array(
        'label' => __('Pijltjes tonen?', '_SBB'),
        'type' => 'switch'
    ),

    'bullets' => array(
        'label' => __('Bolletjes tonen?', '_SBB'),
        'type' => 'switch'
    ),

    'posts_per_row' => array(
        'label' => __('Aantal afbeeldingen per rij?', '_SBB'),
        'type'  => 'slider',
        'value' => 1,
        'properties' => array(
            'min' => 1,
            'max' => 8,
        ),
    ),

    'autoplay' => array(
        'label' => __('Automatisch afspelen?', '_SBB'),
        'value' => 2500,
        'desc' => __('Geef hier aan hoeveel milliseconden een afbeelding wordt weergegeven', '_SBB'),
        'type' => 'text'
    ),

    'slides' => array(
        'type' => 'multi-upload',
        'images_only' => true,
        'label' => __('Afbeeldingen', '_SBB'),
    ),
);

$options = fw_get_defaults($args);