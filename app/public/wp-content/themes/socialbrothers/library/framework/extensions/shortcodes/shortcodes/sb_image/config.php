<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Inhoudselementen', '_SBB'),
		'title'       => __('Afbeelding', '_SBB'),
		'icon'		  => 'fal fa-image',
        'popup_size'  => 'medium',
        'disable_correction' => true,
	)
);
