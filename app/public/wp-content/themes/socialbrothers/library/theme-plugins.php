<?php if (!defined('ABSPATH')) die('Forbidden');

// Here we will define some settings for our Unyson framework, which is mainly the visual page builder.
if (defined('FW')) {
    function fw_framework_change_customizations_dir_rel_path($rel_path)
    {
        return '/library/framework';
    }

    add_filter('fw_framework_customizations_dir_rel_path', 'fw_framework_change_customizations_dir_rel_path');

    function fw_ext_page_builder_content_wrapper_class($class)
    {
        return 'wrapper';
    }

    add_filter('fw_ext_page_builder_content_wrapper_class', 'fw_ext_page_builder_content_wrapper_class');

    function fw_action_theme_include_custom_option_types()
    {
        require_once dirname(__FILE__) . '/framework/includes/option-types/wp-link/class-fw-option-type-wp-link.php';
        require_once dirname(__FILE__) . '/framework/includes/option-types/wp-icon/class-fw-option-type-wp-icon.php';
    }

    add_action('fw_option_types_init', 'fw_action_theme_include_custom_option_types');

    function fw_ext_page_builder_admin_scripts()
    {
        wp_enqueue_style('unyson-vendors', assets('styles/vendors.min.css'));
        wp_enqueue_style('unyson-framework', get_template_directory_uri() . '/library/framework/framework.css');
    }

    add_action('admin_enqueue_scripts', 'fw_ext_page_builder_admin_scripts');

    function fw_get_sizes($default = false)
    {
        $sizes = array(
            'xl' => __('Extra groot', '_SBB'),
            'lg' => __('Groot', '_SBB'),
            'md' => __('Gemiddeld', '_SBB'),
            'sm' => __('Klein', '_SBB'),
            'xs' => __('Extra klein', '_SBB'),
            'none' => __('Geen', '_SBB'),
        );

        if ($default) {
            $sizes[''] = __('Standaard waarde', '_SBB');
        }

        return array_reverse($sizes);
    }

    function fw_get_defaults($options)
    {
        return array(
            array(
                'type' => 'box',
                'options' => array(
                    'general' => array(
                        'type' => 'tab',
                        'title' => __('Algemeen', '_SBB'),
                        'options' => array($options)
                    ),

                    'styling' => array(
                        'type' => 'tab',
                        'title' => __('Opmaak', '_SBB'),
                        'options' => array(
                            'styling_mt' => array(
                                'label' => __('Marge boven', '_SBB'),
                                'type' => 'select',
                                'choices' => fw_get_sizes(true)
                            ),

                            'styling_mb' => array(
                                'label' => __('Marge onder', '_SBB'),
                                'type' => 'select',
                                'choices' => fw_get_sizes(true)
                            ),

                            'styling_pt' => array(
                                'label' => __('Vulling boven', '_SBB'),
                                'type' => 'select',
                                'choices' => fw_get_sizes(true)
                            ),

                            'styling_pb' => array(
                                'label' => __('Vulling onder', '_SBB'),
                                'type' => 'select',
                                'choices' => fw_get_sizes(true)
                            ),

                            'styling_class' => array(
                                'label' => __('CSS klasses', '_SBB'),
                                'type' => 'text',
                            ),

                            'styling_id' => array(
                                'label' => __('CSS ID\'s', '_SBB'),
                                'type' => 'text',
                            ),
                        )
                    )
                )
            )
        );
    }

    function fw_get_defaults_settings($atts, $custom = array())
    {
        $settings = array();

        $styling = array_filter($atts, function ($key) {
            return strpos($key, 'styling_') === 0;
        }, ARRAY_FILTER_USE_KEY);

        foreach ($styling as $key => $value) {
            $key = str_replace('styling_', '', $key);

            if ($value) {
                switch ($key) {
                    case 'mt':
                    case 'mb':
                    case 'pt':
                    case 'pb':
                        $settings['class'][] = $key . '-' . $value;
                        break;
                    case 'class':
                        $settings['class'][] = $value;
                        break;
                    case 'id':
                        $settings['id'][] = $value;
                        break;
                }
            }
        }

        if (is_array($custom))
            $settings = array_merge_recursive($custom, $settings);

        foreach ($settings as $key => $value)
            $settings[$key] = implode(' ', $value);

        $output = fw_attr_to_html($settings);
        return preg_replace('/\s+/', ' ', $output);
    }
}

// Here we will define some settings for Yoast SEO, which is mainly positioning the meta box.
if (defined('WPSEO_VERSION')) {
    function metabox_prio()
    {
        return 'low';
    }

    add_filter('wpseo_metabox_prio', 'metabox_prio');
}

// Here we will define some settings for Advanced Custom Fields, such as saving to a JSON file.
if (class_exists('ACF')) {
    // Save fields as JSON
    function acf_json_save_point($path)
    {
        $path = get_template_directory() . '/library/vendor/advanced-custom-fields';
        return $path;
    }

    add_filter('acf/settings/save_json', 'acf_json_save_point');

    // Load fields as JSON
    function acf_json_load_point($paths)
    {
        unset($paths[0]);
        $paths[] = get_template_directory() . '/library/vendor/advanced-custom-fields';
        return $paths;
    }

    add_filter('acf/settings/load_json', 'acf_json_load_point');

    // Create options page under 'Appearance'
    acf_add_options_sub_page(array(
        'page_title'     => __('Thema', '_SBB'),
        'menu_title'     => __('Thema', '_SBB'),
        'menu_slug'        => 'settings',
        'parent_slug'     => 'options-general.php',
    ));
}

// Here we will define some settings for Gravity Forms, mainly removing the default styling.
if (class_exists('GFCommon')) {
    add_filter('gform_field_input', 'custom_file_upload', 10, 5);
    function custom_file_upload($input, $field, $value, $lead_id, $form_id)
    {
        if ($field->type === 'fileupload') {
            $output = '<div class="ginput_container ginput_container_file">';
            $output .= '<label class="upload">';
            $output .= '<input name="input_' . $field->id . '" id="input_1_' . $field->id . '" type="file" class="upload__select" aria-describedby="validation_message_1_' . $field->id . ' live_validation_message_1_' . $field->id . ' extensions_message_1_' . $field->id . '" onchange="fileupload(this);">';
            $output .= '<span class="upload__button">' . __('Kies een bestand', '_SBF') . '</span>';
            $output .= '<span class="upload__result">' . __('Geen bestand gekozen', '_SBF') . '</span>';
            $output .= '</label>';
            $output .= '</div>';

            $input = $output;
        }

        return $input;
    }

    add_filter('gform_submit_button', 'custom_submit_button', 10, 2);
    function custom_submit_button($button, $form)
    {
        return "<button class='gform_submit' id='gform_submit_button_" . $form['id'] . "'>" . do_shortcode($form['button']['text']) . "</button>";
    }

    add_filter('pre_option_rg_gforms_disable_css', '__return_true');
}


/**
 * adds woocommerce support
 */

if ( 
    in_array( 
      'woocommerce/woocommerce.php', 
      apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) 
    ) 
  ) {
        add_theme_support('woocommerce');
  }


