<?php if (!defined('FW')) die('Forbidden');

class FW_Option_Type_WPLink extends FW_Option_Type
{
    private $fields = array(
        'text' => array(
            'type' => 'text',
            'attr'  => array(
                'placeholder' => 'Hello World!',
                'class' => 'wplink-text',
                'style' => 'margin-bottom: 4px;'
            )
        ),
        'href' => array(
            'type' => 'text',
            'attr'  => array(
                'placeholder' => 'https://www.google.com',
                'class' => 'wplink-href',
                'style' => 'margin-bottom: 4px;'
            )
        ),
        'target' => array(
            'type' => 'checkbox',
            'text' => 'Open the link in a new tab',
            'attr'  => array(
                'class' => 'wplink-target'
            )
        )    
    );

    public function get_type()
    {
        return 'wp-link';
    }

    protected function _enqueue_static($id, $option, $data)
    {
        wp_enqueue_script('wplink');
        wp_enqueue_style('editor-buttons');
    }

    protected function _render($id, $option, $data)
    {
        $html = '<div id="' . $data['id_prefix'] . $id . '">';

        foreach ($this->fields as $key => $value) {
            $html .= fw()->backend->option_type($value['type'])->render($key,
                $value,

                array(
                    'value'    => (isset($data['value'][$key])) ? $data['value'][$key] : $option['value'][$key],
                    'id_prefix'   => $data['id_prefix'] . $id . '-',
                    'name_prefix' => $data['name_prefix'] . '[' . $id . ']',
                )
            );
        }

        $html .= '<button class="wplink-open button media-button button-secondary" style="margin-top: 16px;">Select</button>';
        $html .= '</div>';

        // Load the WP Link and our script so when the user clicks on the button it will open the WP Link popup.
        require_once ABSPATH . "wp-includes/class-wp-editor.php";
        _WP_Editors::wp_link_dialog();

        $html .= '
		 <script type="text/javascript">
	 		jQuery(document).ready(function() {
		 		jQuery(".wplink-open").click(function() {
                    wpActiveEditor = true;
                    wpLink.open("' . $data['id_prefix'] . $id . '");

                    jQuery("input[id=\"wp-link-text\"]").val(jQuery("#' . $data['id_prefix'] . $id . '-text").val());
                    jQuery("input[id=\"wp-link-url\"]").val(jQuery("#' . $data['id_prefix'] . $id . '-href").val());
                    jQuery("input[id=\"wp-link-target\"]").prop("checked", jQuery("#' . $data['id_prefix'] . $id . '-target").is(":checked"));

                    return false;
		 		});

				jQuery("#wp-link-submit").click(function() {
                    var attr = wpLink.getAttrs();
                    attr.target = attr.target == "_target" ? true : false;
                    attr.text = jQuery(this).closest("form").find("#wp-link-text").val();

					if (attr) {
                        jQuery("#' . $data['id_prefix'] . $id . '-text").val(attr.text);
                        jQuery("#' . $data['id_prefix'] . $id . '-href").val(attr.href);
                        jQuery("#' . $data['id_prefix'] . $id . '-target").prop("checked", attr.target);
					}

					wpLink.close();
				});
	 		});
 		</script>';

        return $html;
    }

    protected function _get_value_from_input($option, $input_value)
    {
        if (is_array($input_value)) {
            $value = array();

            $value['text'] = '';
            $value['href'] = '';
            $value['target'] = false;

            if (isset($input_value['text'])) {
                $value['text'] = $input_value['text'];
            }

            if (isset($input_value['href'])) {
                $value['href'] = $input_value['href'];
            }

            if (isset($input_value['target'])) {
                $value['target'] = $input_value['target'];
            }

            return $value;
        }

        return $option['value'];
    }

    protected function _get_defaults()
    {
        return array(
            'value' => array(
                'text' => '',
                'href' => '',
                'target' => false
            )
        );
    }
}

FW_Option_Type::register('FW_Option_Type_WPLink');
