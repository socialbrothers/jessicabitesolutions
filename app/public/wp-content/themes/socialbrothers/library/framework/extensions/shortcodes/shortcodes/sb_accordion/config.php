<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Inhoudselementen', '_SBB'),
		'title'       => __('Accordion', '_SBB'),
		'icon'		  => 'fal fa-caret-circle-down',
		'popup_size'  => 'medium',
		'disable_correction' => true,
	)
);
