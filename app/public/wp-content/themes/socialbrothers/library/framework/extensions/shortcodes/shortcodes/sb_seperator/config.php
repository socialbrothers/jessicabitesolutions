<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Inhoudselementen', '_SBB'),
		'title'       => __('Verdeler', '_SBB'),
		'icon'		  => 'fal fa-ruler-combined',
        'popup_size'  => 'medium',
        'disable_correction' => true,
	)
);
