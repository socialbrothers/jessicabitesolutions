<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Inhoudselementen', '_SBB'),
		'title'       => __('Berichten', '_SBB'),
		'icon'		  => 'fal fa-grip-horizontal',
        'popup_size'  => 'medium',
        'disable_correction' => true,
	)
);
