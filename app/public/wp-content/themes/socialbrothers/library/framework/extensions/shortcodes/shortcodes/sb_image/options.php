<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'image' => array(
        'type' => 'upload',
        'label' => __('Afbeelding', '_SBB'),
        'images_only' => true,
    )
);

$options = fw_get_defaults($args);