<div class="container">
    <?php if (have_posts()) : ?>
        <div class="row">
            <div class="col-lg-12">
                <h1>
                    <?php echo sprintf(__('Zoekresultaten voor: %s', '_SBF'), $_GET['s']); ?>
                    <small><?php echo sprintf(__('%s resultaten gevonden.', '_SBF'), $GLOBALS['wp_query']->post_count); ?></small>
                </h1>
            </div>

            <div class="col-lg-12">
                <?php get_template_part('templates/component/component', 'search'); ?>
            </div>

            <div class="col-lg-12">
                <div class="search-results">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="search-result">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="row">
            <div class="col-lg-12">
                <h1><?php echo __('Geen resultaten gevonden!', '_SBF'); ?></h1>
                <p><?php echo __('Helaas zijn er met de door u gekozen zoektermen geen resultaten gevonden. U kunt het opnieuw proberen met het onderstaande formulier of aan de hand van het menu aan de bovenzijde van de website.', '_SBF'); ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <?php get_template_part('templates/component/component', 'search'); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
