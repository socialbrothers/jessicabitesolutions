/**
 * Accordion
 */
function accordion() {
	$(document).on('click', '.accordion .accordion__items .accordion__item .accordion__header', function () {
		if (
			$(this)
				.parent()
				.hasClass('accordion__item--active')
		) {
			$(this)
				.parent()
				.removeClass('accordion__item--active')
				.find('.accordion__content')
				.stop()
				.slideUp(250);
		} else {
			$('.accordion .accordion__item--active')
				.removeClass('accordion__item--active')
				.find('.accordion__content')
				.stop()
				.slideUp(250);

			$(this)
				.parent()
				.addClass('accordion__item--active');

			$(this)
				.parent()
				.find('.accordion__content')
				.stop()
				.slideDown(250);
		}
	});
}

accordion();
