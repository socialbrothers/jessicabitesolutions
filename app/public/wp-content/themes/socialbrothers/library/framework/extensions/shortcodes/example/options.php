<?php if (!defined('FW')) die('Forbidden');

$options = fw_get_defaults(
    array(
        'example' => array(
            'label' => __('Voorbeeld', '_SBB'),
            'type' => 'text',
        ),
    )
);