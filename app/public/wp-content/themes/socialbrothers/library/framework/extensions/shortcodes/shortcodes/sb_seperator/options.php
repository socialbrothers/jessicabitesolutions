<?php if (!defined('FW')) die('Forbidden');

$args = array(
	'size'  => array(
		'label' => __('Hoogte', '_SBB'),
		'type' => 'select',
		'choices' => fw_get_sizes()
	),

	'line' => array(
		'label'		=> __('Lijn weergeven', '_SBB'),
		'value'		=> false,
		'desc'		=> __('Wilt u dat de sectie verdeler een lijn in het midden heeft?', '_SBB'),
		'type'      => 'switch',
    )
);

$options = fw_get_defaults($args);