<?php if (!defined('FW')) die('Forbidden');

function _filter_disable_default_shortcodes($to_disable) {
    $to_disable = array('text_block', 'divider', 'accordion', 'button', 'calendar', 'call_to_action', 'icon_box', 'map', 'media_image', 'media_video', 'notification', 'special_heading', 'table', 'tabs', 'team-member', 'testimonials', 'widget_area', 'team_member', 'contact_form', 'icon');

    return $to_disable;
}

add_filter('fw_ext_shortcodes_disable_shortcodes', '_filter_disable_default_shortcodes');