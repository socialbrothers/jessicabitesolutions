module.exports = {
    name: "Jessica Bite Solutions",
    proxy: "http://jessica-bite-solutions.local/",
    vendors: {
        js: [
            "../node_modules/jquery/dist/jquery.min.js",
            "../node_modules/swiper/swiper-bundle.min.js",
            "../node_modules/object-fit-images/dist/ofi.min.js",
        ],

        css: [
            "../node_modules/swiper/swiper-bundle.min.css",
            "../node_modules/@fortawesome/fontawesome-pro/css/all.min.css",
        ],

        fonts: ["../node_modules/@fortawesome/fontawesome-pro/webfonts/*"],
    },
};