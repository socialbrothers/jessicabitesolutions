<?php if (!defined('FW')) die('Forbidden');

if (class_exists('GFCommon')) {
	$forms = array();

	foreach (GFAPI::get_forms() as $form) {
		$forms[$form['id']] = $form['title'];
	}

	$args = array(
		'form' => array(
			'label' => __('Selecteer een formulier', '_SBB'),
			'type' => 'select',
			'choices' => $forms,
		),
	
		'title' => array(
			'label' => __('Titel tonen?', '_SBB'),
			'type' => 'switch',
		),
	
		'description' => array(
			'label' => __('Omschrijving tonen?', '_SBB'),
			'type' => 'switch',
		),
	
		'ajax' => array(
			'label' => __('Verzenden zonder pagina te herladen?', '_SBB'),
			'type' => 'switch',
		),
	);
	
	$options = fw_get_defaults($args);
}