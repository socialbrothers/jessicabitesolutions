<?php if (!defined('ABSPATH')) die('Forbidden');

/**
 * This function will add custom scripts and styles to our WordPress website.
 * It uses the function wp_head to include them on the right time.
 *
 * @return null
 */
function scripts()
{
	// Stylesheet
	wp_enqueue_style('vendors', assets('styles/vendors.min.css'), array(), filetime(get_template_directory() . '/assets/styles/vendors.min.css'));
	wp_enqueue_style('styles', assets('styles/styles.min.css'), array(), filetime(get_template_directory() . '/assets/styles/styles.min.css'));

	// Scripts
	wp_enqueue_script('vendors', assets('scripts/vendors.min.js'), NULL, filetime(get_template_directory() . '/assets/scripts/vendors.min.js'));
	wp_enqueue_script('scripts', assets('scripts/scripts.min.js'), NULL, filetime(get_template_directory() . '/assets/scripts/scripts.min.js'), true);

	// Setttings
	wp_localize_script('scripts', 'theme', array('ajax_url' => admin_url('admin-ajax.php')));

	// Disable
	wp_dequeue_style('wp-block-library');
	wp_deregister_style('wp-block-library');
}

/**
 * This function loads scripts and styles in the WordPress admin backend,
 * not in the front-end of the website!
 */
function admin_scripts()
{ }

/**
 * This functions will change the link on the logo of the WordPress Backend
 * Login screen. Normally it goes to WordPress.org but now it goes to our website!
 *
 * @return string
 */
function login_url()
{
	return home_url();
}

/**
 * This function will remove the top margin for the admin bar so we can
 * see the style of our header.
 *
 * @return null
 */
function filter_head()
{
	remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'filter_head');

/**
 * This function will allow you to upload SVG images using the WordPress
 * media library.
 *
 * @return array
 */
function upload_mime_types($mimes)
{
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

add_filter('upload_mimes', 'upload_mime_types');

/**
 * This functions add the Walker_Bem() class to all the WordPress menu's
 * using the wp_nav_menu_args hook.
 * 
 * @link https://developer.wordpress.org/reference/hooks/wp_nav_menu_args/
 * 
 * @return array;
 */
function wp_nav_menu_args_bem($args) 
{
    return array_merge($args, array(
		'container' => false,
		'walker' => new Walker_Bem(),
    ));
}

add_filter('wp_nav_menu_args', 'wp_nav_menu_args_bem');

/**
 * This function will remove/add items to the WordPress admin
 * menu using the admin_menu action
 */
function custom_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

add_action( 'admin_menu', 'custom_admin_menus' );


// Check if search engines are blocked
function sb_SearchEngineBlocked()
{
	$url = site_url();

	$curdate = date('dmy');

	$isblocked = get_option('blog_public');
	$optiondate = get_option('lastcron');

	$isblocked = intval($isblocked);


	if ($optiondate != $curdate && $isblocked == 0) {
		update_option('lastcron', $curdate);

		$recipients = array(
			'wordpress@socialbrothers.nl',
			'steven@socialbrothers.nl',
			'rob@socialbrothers.nl'
		);
		wp_mail($recipients, 'Search engine blocked', 'Search engine blocked on: ' . $url);

		ob_start();
		
		$retval = ob_get_clean();
	}
}

add_action('admin_head', 'sb_SearchEngineBlocked');