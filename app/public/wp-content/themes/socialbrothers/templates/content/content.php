<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php sprintf(__('Maak een bestand aan genaamd "content-%s.php" in de map "templates/content/" om een specifieke HTML structuur aan je bestand mee te geven. Zo blijven alle post types mooi gescheiden en is de hoofd map goed opgeruimd.', '_SBF'), get_post_type_object(get_post_type())->rewrite['slug']); ?>
        </div>
    </div>
</div>
